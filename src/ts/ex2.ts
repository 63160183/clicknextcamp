interface Question {
    question: string;
    choices: string[];
    correctAnswer: number;
}

const questions: Question[] = [
    {
        question: 'What is the output of the following code?\n\nconsole.log(typeof null);',
        choices: ['"object"', '"null"', '"undefined"', '"boolean"'],
        correctAnswer: 0,
    },
    {
        question: 'Which method is used to add one or more elements to the end of an array?',
        choices: ['push()', 'join()', 'slice()', 'concat()'],
        correctAnswer: 0,
    },
    {
        question: 'What is the result of the following expression?\n\n3 + 2 + "7"',
        choices: ['"327"', '"12"', '"57"', '"NaN"'],
        correctAnswer: 2,
    },
    {
        question: 'What is the purpose of the "use strict" directive in JavaScript?',
        choices: ['Enforce stricter type checking', 'Enable the use of modern syntax', 'Enable strict mode for improved error handling', 'Disable certain features for better performance'],
        correctAnswer: 2,
    },
    {
        question: 'What is the scope of a variable declared with the "let" keyword?',
        choices: ['Function scope', 'Global scope', 'Block scope', 'Module scope'],
        correctAnswer: 2,
    },
    {
        question: 'Which higher-order function is used to transform elements of an array into a single value?',
        choices: ['map()', 'filter()', 'reduce()', 'forEach()'],
        correctAnswer: 2,
    },
    {
        question: 'What does the "=== " operator in JavaScript check for?',
        choices: ['Equality of values', 'Equality of values and types', 'Inequality of values', 'Reference equality'],
        correctAnswer: 1,
    },
    {
        question: 'What is the purpose of the "this" keyword in JavaScript?',
        choices: ['Refer to the current function', 'Refer to the parent function', 'Refer to the global object', 'Refer to the object that owns the current code'],
        correctAnswer: 3,
    },
    {
        question: 'What does the "NaN" value represent in JavaScript?',
        choices: ['Not a Number', 'Null', 'Negative Number', 'Not Applicable'],
        correctAnswer: 0,
    },
    {
        question: 'Which method is used to remove the last element from an array?',
        choices: ['pop()', 'shift()', 'slice()', 'splice()'],
        correctAnswer: 0,
    },
];

const root: HTMLDivElement = document.getElementById('root') as HTMLDivElement;
const scoreText = document.createElement("p");
const test = document.createElement("div");
const resultText = document.createElement("p");

let score = 0;
scoreText.innerText = "Current Score: " + score + "/10";
test.appendChild(scoreText);
test.appendChild(resultText);

for (let i in questions) {
    const questionContainer = document.createElement("div");
    const p = document.createElement('p');
    p.innerText = questions[i].question;
    questionContainer.appendChild(p);

    for (let j in questions[i].choices) {
        const radio = document.createElement("INPUT");
        radio.setAttribute("type", "radio");
        radio.setAttribute("name", "question" + i);
        radio.setAttribute("value", j);
        const label = document.createElement('label');
        const br = document.createElement('br');
        label.innerText = questions[i].choices[j];
        questionContainer.appendChild(radio);
        questionContainer.appendChild(label);
        questionContainer.appendChild(br);
    }

    const button = document.createElement("button");
    button.textContent = "Submit";
    questionContainer.appendChild(button);

    button.addEventListener("click", function () {
        const choiceSelect = questionContainer.querySelector(`input[name=question${parseInt(i)}]:checked`) as HTMLInputElement;
        if (choiceSelect) {
            const answer = parseInt(choiceSelect.value);
            if (answer === questions[i].correctAnswer) {
                score++;
                scoreText.textContent = "Current Score: " + score + "/10";
                const result = document.createElement("p");
                result.textContent = "Correct!";
                questionContainer.appendChild(result);
            } else {
                const result = document.createElement("p");
                result.textContent = "Inorrect!";
                questionContainer.appendChild(result);
            }
            button.disabled = true;
        } else {
            alert("please choose an answer first!");
        }
    });
    test.appendChild(questionContainer);
}
root.appendChild(test);
export { }