interface Menu {
    name: string 
    subMenu: SubMenu[]
  }
   
  interface SubMenu {
    name: string
  }
   
  const menus: Menu[] = [
    {
      name: 'Home',
      subMenu: [],
    },
    {
      name: 'About',
      subMenu: [
        {
          name: 'Company',
        },
        {
          name: 'Team',
        },
      ],
    },
    {
      name: 'Products',
      subMenu: [
        {
          name: 'Electronics',
        },
        {
          name: 'Clothing',
        },
        {
          name: 'Accessories',
        },
      ],
    },
    {
      name: 'Services',
      subMenu: [],
    },
    {
      name: 'Contact',
      subMenu: [
        {
          name: 'Phone',
        },
      ],
    },
    {
      name: 'Blog',
      subMenu: [],
    },
    {
      name: 'Gallery',
      subMenu: [
        {
          name: 'Photos',
        },
        {
          name: 'Videos',
        },
        {
          name: 'Events',
        },
      ],
    },
    {
      name: 'FAQ',
      subMenu: [],
    },
    {
      name: 'Downloads',
      subMenu: [
        {
          name: 'Documents',
        },
        {
          name: 'Software',
        },
      ],
    },
    {
      name: 'Support',
      subMenu: [
        {
          name: 'Help Center',
        },
        {
          name: 'Contact Us',
        },
        {
          name: 'Knowledge Base',
        },
      ],
    },
  ];

  const show: HTMLDivElement = document.getElementById('show') as HTMLDivElement
  const rootmenu: HTMLDivElement = document.getElementById('rootmenu') as HTMLDivElement

  let x = ''
for (var i in menus) {
    const li = document.createElement('li')
    li.innerText = menus[i].name
    rootmenu.appendChild(li) 
    const ul = document.createElement('ul')
    li.appendChild(ul)
    for(var j in menus[i].subMenu){
        const li = document.createElement('li')
        li.innerText = menus[i].subMenu[j].name
        ul.appendChild(li)
    }
}

  export{}